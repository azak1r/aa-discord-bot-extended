from django.apps import AppConfig


class AADiscordBotExtendedConfig(AppConfig):
    name = "aadiscordbotextended"
    label = "aadiscordbotextended"
    verbose_name = "AA Discord Bot - Extended"
