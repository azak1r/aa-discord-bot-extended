# Cog Stuff
import logging

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from allianceauth.services.modules.discord.models import DiscordUser
from app_utils.urls import static_file_absolute_url

logger = logging.getLogger(__name__)


class Status(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def status(self, ctx):

        try:
            id = ctx.message.author.id
            discord_user = DiscordUser.objects.get(uid=id)

            if discord_user:
                discord_active = True

            logger.error("Discord service is active for this user")

            embed = Embed(title="Account synchronized with AUTH")
            embed.colour = Color.green()
            embed.add_field(name="Synced status", value=discord_active, inline=True)
            embed.add_field(name="User", value=ctx.message.author, inline=True)
            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )
            embed.description = (
                "The account you are using is linked to a Council AUTH user."
            )

            await ctx.reply(embed=embed)

        except Exception:
            discord_active = False

            logger.error("Discord service is not active for this user")

            embed = Embed(title="Account not synchronized with AUTH")
            embed.colour = Color.red()
            embed.add_field(name="Synced status", value=discord_active, inline=True)
            embed.add_field(name="User", value=ctx.message.author, inline=True)
            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )
            embed.description = "Activate the to access this command. See `!howto discord` and !howto fixdiscod` for more help."

            await ctx.reply(embed=embed)


def setup(bot):
    bot.add_cog(Status(bot))
