# Cog Stuff
import logging
from datetime import timedelta

import discord
from afat.models import AFat, AFatLink
from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from django.contrib.auth.models import User
from django.utils import timezone

from aadiscordbotextended.models import FcStatsCog

from allianceauth.services.modules.discord.models import DiscordUser

from app_utils.urls import static_file_absolute_url

from allianceauth.authentication.models import CharacterOwnership

logger = logging.getLogger(__name__)

class FatStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def fatstats(self, ctx, user: discord.Member = None):

        user_argument = ctx.message.content[10:]

        auth_user = False

        if not user:

            try:
                author_id = ctx.message.author.id

                discord_user = DiscordUser.objects.get(uid=author_id)
            
            except DiscordUser.DoesNotExist:

                embed = Embed(title="Fat statistics not available")
                embed.colour = Color.red()
                embed.description = "The author of this command has no active discord service linked to their account. Check !status for more info or provide an user argument for the command."
                embed.set_thumbnail(
                    url=static_file_absolute_url("aadiscordbotextended/terminate.png")
                )

                await ctx.reply(embed=embed)

            else:  

                auth_user = discord_user.user

        else:

            try:
                author_id = user.id

                discord_user = DiscordUser.objects.get(uid=author_id)
            
            except DiscordUser.DoesNotExist:

                embed = Embed(title="Fat statistics not available")
                embed.colour = Color.red()
                embed.description = "The author of this command has no active discord service linked to their account. Check !status for more info or provide an user argument for the command."
                embed.set_thumbnail(
                    url=static_file_absolute_url("aadiscordbotextended/terminate.png")
                )

                await ctx.reply(embed=embed)

            else:  

                auth_user = discord_user.user

        
        if auth_user:
            
            characters = CharacterOwnership.objects.filter(user=auth_user).values_list("character", flat=True)

            main_character = auth_user.profile.main_character.character_name

            fat_links = AFat.objects.filter(character__in=characters).values_list("id", flat=True)

            grip = AFatLink.objects.filter(fleet__icontains="grip", id__in=fat_links)

            if grip:
                grip_completed = True
            else:
                grip_completed = False

            embed = Embed(title="Fat statistics for {0}".format(main_character))
            embed.colour = Color.green()
            embed.add_field(name="Fat count:", value="{0}".format(fat_links.count()),inline=True)
            embed.add_field(name="GRIP completed:", value=grip_completed)

            await ctx.reply(embed=embed)

def setup(bot):
    bot.add_cog(FatStats(bot))
