# Cog Stuff
import logging

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from app_utils.urls import static_file_absolute_url

from aadiscordbotextended.models import Embed as EmbedLines
from aadiscordbotextended.models import HowToCog

logger = logging.getLogger(__name__)


class Howto(commands.Cog):
    """
    All about me!
    """

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def howto(self, ctx):
        """
        All about the bot
        """
        await ctx.trigger_typing()

        user_argument = ctx.message.content[7:]

        if not user_argument:

            arguments = HowToCog.objects.all()

            arguments = _arguments(arguments)

            embed = Embed(title="Error while running command")
            embed.colour = Color.red()

            embed.set_thumbnail(
                url=static_file_absolute_url("aadiscordbotextended/terminate.png")
            )

            embed.description = (
                "Missing argument for command. Available arguments are: *{}*".format(
                    arguments
                )
            )

            await ctx.reply(embed=embed)

        else:

            try:
                howto = HowToCog.objects.get(title=user_argument)

                embed_lines = EmbedLines.objects.all().filter(
                    howtocog__title=user_argument
                )

                embed = Embed(title="How to: {}".format(user_argument))

                embed.colour = Color.blue()

                embed.description = howto.description

                embed.set_author(name=howto.author)

                embed.url = howto.url

                if howto.image:
                    embed.set_image(url=howto.image)

                embed.set_thumbnail(
                    url=static_file_absolute_url("aadiscordbotextended/help.png")
                )

                for x in embed_lines:
                    embed.add_field(
                        name=x.title,
                        value=x.description,
                        inline=False,
                    )

                await ctx.reply(embed=embed)

            except HowToCog.DoesNotExist:
                arguments = HowToCog.objects.all()

                arguments = _arguments(arguments)

                embed = Embed(title="Error while running command")
                embed.colour = Color.red()

                embed.set_thumbnail(
                    url=static_file_absolute_url("aadiscordbotextended/terminate.png")
                )
                embed.description = (
                    "Argument not found. Available arguments are: *{}*".format(
                        arguments
                    )
                )

                await ctx.reply(embed=embed)


def setup(bot):
    bot.add_cog(Howto(bot))


def _arguments(arguments):
    arguments = [x.title for x in arguments]

    if arguments:
        return ", ".join(arguments)
    else:
        return None
