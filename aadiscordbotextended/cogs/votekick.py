# Cog Stuff
import logging

import discord
from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from app_utils.urls import static_file_absolute_url

from aadiscordbotextended.app_settings import (
    AADISCORDBOTEXTENDED_VOTEKICK_ROLES,
    AADISCORDBOTEXTENDED_VOTEKICK_VOTES_NEEDED,
)

logger = logging.getLogger(__name__)


class VoteKick(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="votekick")
    async def votekick(self, ctx, user: discord.Member = None, *, reason):

        emoji = "✅"

        roles = ctx.message.author.roles

        voting_roles = ",".join(AADISCORDBOTEXTENDED_VOTEKICK_ROLES)

        if _can_initiate_votekick(roles):
            if not _can_initiate_votekick(user.roles):
                embed = Embed(title="Votekick Initiated")
                embed.colour = Color.orange()
                embed.description = "A vote has been initiated to kick {0} from the server. A total of {1} individual votes are needed to kick the member. Please react to this message to vote".format(
                    user.mention, AADISCORDBOTEXTENDED_VOTEKICK_VOTES_NEEDED
                )
                embed.add_field(name="User ID", value=user, inline=True)
                embed.add_field(name="Reason", value=reason, inline=True)
                embed.add_field(
                    name="Role needed to vote", value=voting_roles, inline=True
                )

                msg = await ctx.message.channel.send(embed=embed)

                await msg.add_reaction(emoji)

                reaction, voter = await self.bot.wait_for(
                    "reaction_add",
                    check=check_count_reaction(
                        "✅", AADISCORDBOTEXTENDED_VOTEKICK_VOTES_NEEDED + 1, msg
                    ),
                )

                embed = Embed(title="Votekick passed")
                embed.colour = Color.green()
                embed.description = "Vote passed. User has been kicked from the server."
                embed.set_thumbnail(
                    url=static_file_absolute_url("opcalendar/terminate.png")
                )
                embed.add_field(name="User", value=user)
                embed.add_field(name="Reason", value=reason)

                await ctx.message.channel.send(embed=embed)

                await user.ban(reason=reason)
            else:
                embed = Embed(title="Votekick initation failed")
                embed.colour = Color.red()
                embed.description = "{0} has vote kick powers. You can't initiate a vote kick against such users. Contact server admin to kick the user.".format(
                    user.mention
                )

                await ctx.message.channel.send(embed=embed)

        else:
            embed = Embed(title="Votekick initation failed")
            embed.colour = Color.red()
            embed.description = (
                "You do not have the required roles to initiate a vote kick."
            )

            await ctx.message.channel.send(embed=embed)


def _can_initiate_votekick(roles):
    role_names = [role.name.lower() for role in roles]

    return any(x in AADISCORDBOTEXTENDED_VOTEKICK_ROLES for x in role_names)


def check_count_reaction(emoji, desired_count, message):
    def predicate(reaction, voter):
        if _can_initiate_votekick(voter.roles):
            return (
                reaction.message == message
                and reaction.emoji == emoji
                and reaction.count >= desired_count
            )

    return predicate


def setup(bot):
    bot.add_cog(VoteKick(bot))
