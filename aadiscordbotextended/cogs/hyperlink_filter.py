import logging
import re

from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from app_utils.urls import static_file_absolute_url

from aadiscordbotextended.app_settings import (
    AADISCORDBOTEXTENDED_HYPERLINK_LIMIT,
    AADISCORDBOTEXTENDED_HYPERLINK_ROLES,
)

logger = logging.getLogger(__name__)

ban_warning_list = []


class HyperlinkFilterCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        is_bot = message.author.bot

        is_webhook = bool(message.webhook_id)

        # Is the message author a bot or a hyperlink
        if not is_bot or is_webhook:

            author_id = message.author.id

            roles = message.author.roles

            # Does the message have an hyperlink
            if _match_url(message.content):
                # Does the author have a member title
                if not _can_post_hyperlinks(roles):

                    await message.delete()
                    # Add author id to ban warning list
                    ban_warning_list.append(author_id)
                    if (
                        ban_warning_list.count(author_id)
                        < AADISCORDBOTEXTENDED_HYPERLINK_LIMIT
                    ):
                        embed = Embed(title="Warning: Hyperlinks filtered")
                        embed.colour = Color.red()
                        embed.set_thumbnail(
                            url=static_file_absolute_url("opcalendar/terminate.png")
                        )
                        embed.description = "Hyperlinks are only allowed from members. Please authenticate. For more info see `!howto discord`"
                        embed.add_field(
                            name="Warnings",
                            value="{0} of {1}".format(
                                ban_warning_list.count(author_id),
                                AADISCORDBOTEXTENDED_HYPERLINK_LIMIT,
                            ),
                        )
                        embed.add_field(name="User", value="{0}".format(message.author))

                        await message.channel.send(embed=embed, delete_after=10)

                    else:
                        embed = Embed(title="User kicked")
                        embed.colour = Color.red()
                        embed.set_thumbnail(
                            url=static_file_absolute_url("opcalendar/terminate.png")
                        )
                        embed.description = "User kicked for hyperlink spam"
                        embed.add_field(
                            name="Warnings",
                            value="{0}".format(ban_warning_list.count(author_id)),
                        )
                        embed.add_field(name="User", value="{0}".format(message.author))

                        await message.channel.send(embed=embed)

                        # Notify user for the kick
                        await message.author.send(
                            "You have been kicked from the discord server for hyperlink spamming. Authentication is required to post hyperlinks."
                        )

                        # Kick the user
                        await message.author.kick(reason="Hyperlink spamming")


def _match_url(url):
    regex = re.compile(
        r"(([\w]+:)?//)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,63}(:[\d]+)?(/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?"
    )
    if regex.match(url):
        return True
    else:
        return False


def _can_post_hyperlinks(roles):
    role_names = [role.name.lower() for role in roles]

    return any(x in AADISCORDBOTEXTENDED_HYPERLINK_ROLES for x in role_names)


def setup(bot):
    bot.add_cog(HyperlinkFilterCog(bot))
