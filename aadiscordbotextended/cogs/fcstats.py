# Cog Stuff
import logging
from datetime import timedelta

from afat.models import AFatLink
from discord.colour import Color
from discord.embeds import Embed
from discord.ext import commands

from django.contrib.auth.models import User
from django.utils import timezone

from aadiscordbotextended.models import FcStatsCog

logger = logging.getLogger(__name__)


class FcStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def fcstats(self, ctx):

        user_argument = ctx.message.content[9:]

        fc_roles = FcStatsCog.objects.values_list("group__name", flat=True).distinct()

        fcs = User.objects.filter(groups__name__in=fc_roles)

        stats = dict()

        description = str()

        footer = "Fat links for roles: {0}".format(", ".join(fc_roles))

        if _isNum(user_argument):

            title = "Fat link statistics for the past {0} days".format(user_argument)

        else:

            title = "Fat link statistics for all time"

        for fc in fcs:

            fat_links = AFatLink.objects.filter(creator=fc)

            if _isNum(user_argument):

                timeframe = timezone.now() - timedelta(days=int(user_argument))

                fat_links = fat_links.filter(afattime__gte=timeframe)

            stats[fc.profile.main_character.character_name] = fat_links.count()

        sort_fats = sorted(stats.items(), key=lambda x: x[1], reverse=True)

        for key, stats in sort_fats:

            description += f"{key}: {stats}\n"

        embed = Embed(title=title)
        embed.colour = Color.blue()
        embed.description = description
        embed.set_footer(text=footer)

        await ctx.reply(embed=embed)


def _isNum(data):
    try:
        int(data)
        return True
    except ValueError:
        return False


def setup(bot):
    bot.add_cog(FcStats(bot))
