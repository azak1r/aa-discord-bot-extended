from django.contrib.auth.models import Group
from django.db import models

# Create your models here.


class General(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False
        default_permissions = ()
        permissions = (("basic_access", "Can access this app"),)


class Embed(models.Model):

    title = models.TextField(
        blank=True,
        max_length=256,
        help_text="Title line for this embed line",
    )

    description = models.TextField(
        default="",
        max_length=4096,
        help_text="Body content for this embed line",
    )

    inline = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.title)


class HowToCog(models.Model):

    title = models.CharField(
        max_length=256,
        help_text="The argument that is used to call this command",
    )

    description = models.TextField(
        blank=True,
        help_text="Description content",
    )

    image = models.URLField(
        max_length=256,
        blank=True,
        help_text="Url for the image",
    )

    embed = models.ManyToManyField(
        Embed,
        help_text="Additional embed fields to add",
    )

    author = models.CharField(
        max_length=64,
        blank=True,
        help_text="Author to be displayed",
    )

    url = models.URLField(
        max_length=256,
        blank=True,
        help_text="Hyperlink used",
    )

    enabled = models.BooleanField(default=True, help_text="Is this command active?")

    def __str__(self):
        return "{}".format(self.title)


class FcStatsCog(models.Model):

    group = models.ForeignKey(
        Group,
        on_delete=models.deletion.CASCADE,
    )
