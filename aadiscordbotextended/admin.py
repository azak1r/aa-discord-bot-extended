from django.contrib import admin

from aadiscordbotextended.forms import EmbedAdminForm, HowToCogAdminForm
from aadiscordbotextended.models import Embed, FcStatsCog, HowToCog


@admin.register(HowToCog)
class HowToCogAdmin(admin.ModelAdmin):
    form = HowToCogAdminForm
    list_display = (
        "title",
        "enabled",
        "_embed",
    )

    @classmethod
    def _embed(cls, obj):
        titles = [x.title for x in obj.embed.all().order_by("title")]

        if titles:
            return ", ".join(titles)
        else:
            return None

    _embed.short_description = "Restricted to"
    _embed.admin_order_field = "embed__title"


@admin.register(Embed)
class EmbedAdmin(admin.ModelAdmin):
    form = EmbedAdminForm
    list_display = ("title",)


admin.site.register(FcStatsCog)
