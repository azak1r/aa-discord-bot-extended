from django.conf import settings

from app_utils.django import clean_setting


# Use a small helper to check if AA-Discordbot is installs
def discord_bot_active():
    return "aadiscordbot" in settings.INSTALLED_APPS


AADISCORDBOTEXTENDED_HYPERLINK_LIMIT = clean_setting(
    "AADISCORDBOTEXTENDED_HYPERLINK_LIMIT", 3
)

AADISCORDBOTEXTENDED_VOTEKICK_VOTES_NEEDED = clean_setting(
    "AADISCORDBOTEXTENDED_VOTEKICK_VOTES_NEEDED", 3
)

AADISCORDBOTEXTENDED_HYPERLINK_ROLES = clean_setting(
    "AADISCORDBOTEXTENDED_HYPERLINK_ROLES", ["member", "guest"]
)

AADISCORDBOTEXTENDED_VOTEKICK_ROLES = clean_setting(
    "AADISCORDBOTEXTENDED_VOTEKICK_ROLES", ["ceo", "director"]
)
